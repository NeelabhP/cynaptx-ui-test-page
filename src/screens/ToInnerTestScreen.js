import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import PropTypes from 'prop-types';

import AppHeader from '../components/AppHeader';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  screenNameText: {
    fontSize: 24,
    fontWeight: 'bold',
    marginVertical: 20,
  },
});

class ToInnerTestScreen extends React.Component {
  static navigationOptions = {
    headerTitle: <AppHeader />,
    headerBackTitle: 'Inner Test Screen',
  };

  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.screenNameText}>To Inner Test Screen</Text>
        <Button
          title="The Inner Screen"
          onPress={
            () => {
              this.props.navigation.navigate("TestInnerScreen")
            }
          }
        />
      </View>
    );
  }
}

export default ToInnerTestScreen;
