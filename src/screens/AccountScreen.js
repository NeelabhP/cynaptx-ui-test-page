import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import PropTypes from 'prop-types';

import AppHeader from '../components/AppHeader';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  screenNameText: {
    fontSize: 24,
    fontWeight: 'bold',
  },
});

class AccountScreen extends React.Component {
  static navigationOptions = {
    headerTitle: <AppHeader />,
    headerBackTitle: 'Account',
  };

  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.screenNameText}>Account Screen</Text>
      </View>
    );
  }
}

export default AccountScreen;
