import React from "react";
import { Platform } from "react-native";
import { createStackNavigator, createBottomTabNavigator, createSwitchNavigator } from "react-navigation";
import Ionicons from "react-native-vector-icons/Ionicons";

import TestMainScreen from "./screens/TestMainScreen";

import ToInnerTestScreen from "./screens/ToInnerTestScreen";
import TestInnerScreen from "./screens/TestInnerScreen";

import DocumentsScreen from "./screens/DocumentsScreen";

import AccountScreen from "./screens/AccountScreen";

const TestMainStack = createStackNavigator({
    TestMain: {
        screen: TestMainScreen,
    },
});

const ToInnerTestStack = createStackNavigator({
    ToInnerTest: {
        screen: ToInnerTestScreen,
    },
    TestInnerScreen: {
      screen: TestInnerScreen,
    }
});

const DocumentsStack = createStackNavigator({
    Documents: {
        screen: DocumentsScreen,
    },
});

const AccountStack = createStackNavigator({
    Account: {
        screen: AccountScreen,
    },
});

const AppStack = createBottomTabNavigator({
    TestMain: {
        screen: TestMainStack,
    },
    ToInnerTest: {
        screen: ToInnerTestStack,
        navigationOptions: {
            tabBarLabel: "To Inner Test Screen",
        },
    },
    Documents: {
        screen: DocumentsStack,
    },
    Account: {
        screen: AccountStack,
    },
},
{
    navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ tintColor }) => { // eslint-disable-line
            const { routeName } = navigation.state;

            let iconName;
            let iconSize = Platform.OS === "ios" ? 28 : 30;

            switch (routeName) {
                case "TestMain":
                    iconName = Platform.OS === "ios" ? "ios-home-outline" : "md-home";
                    break;
                case "ToInnerTest":
                    iconName = Platform.OS === "ios" ? "ios-briefcase-outline" : "md-briefcase";
                    break;
                case "Documents":
                    iconName = Platform.OS === "ios" ? "ios-document-outline" : "md-document";
                    break;
                case "Account":
                    // Appears smaller on ios compared to other icons
                    iconName = Platform.OS === "ios" ? "ios-person-outline" : "md-person";
                    iconSize = Platform.OS === "ios" ? 33 : 30;
                    break;
                default:
                    iconName = null;
                    iconSize = null;
                    break;
            }

            return <Ionicons name={ iconName } size={ iconSize } color={ tintColor } />;
        },
    }),
    tabBarOptions: {
        activeTintColor: "tomato",
        inactiveTintColor: "grey",
        style: {
            backgroundColor: "white",
            shadowOffset: {
                width: 5,
                height: 3,
            },
            shadowColor: "black",
            shadowOpacity: 0.5,
            elevation: 1,
        },
    },
    initialRouteName: "ToInnerTest"
});

export default AppStack;