import React from 'react';
import { View, Image, Platform } from 'react-native';

const AppHeader = () => (
  <View
    style={{
      flex: 1,
      flexDirection: 'row',
      // backgroundColor: "black",
      marginTop: Platform.OS === 'ios' ? -10 : 0,
      justifyContent: 'center',
    }}>
    <Image
      source={require('../../assets/images/logos/headerLogo.png')}
      style={{
        width: 120,
        height: 60,
        resizeMode: 'contain',
        // backgroundColor: "red",
      }}
    />
  </View>
);

export default AppHeader;
